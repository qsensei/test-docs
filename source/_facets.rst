Facets
******

Getting all Users
-----------------

.. http:get:: /users/derp/(herp)

    Get all Users

    **Example request**

    .. sourcecode:: http

        GET /users/123

    **Example response**

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Vary: Accept
        Content-Type: text/javascript

        [
            {
                "post_id": 12345,
                "author_id": 123,
                "tags": ["server", "web"],
                "subject": "I tried Nginx"
            },
            {
                "post_id": 12346,
                "author_id": 123,
                "tags": ["html5", "standards", "web"],
                "subject": "We go to HTML 5"
            }
        ]

Deleting a Person
-----------------

.. http:delete:: /users/derp/(herp)

    Get all Users

    **Example request**

    .. sourcecode:: http

        GET /users/123

    **Example response**

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Vary: Accept
        Content-Type: text/javascript

        [
            {
                "post_id": 12345,
                "author_id": 123,
                "tags": ["server", "web"],
                "subject": "I tried Nginx"
            },
            {
                "post_id": 12346,
                "author_id": 123,
                "tags": ["html5", "standards", "web"],
                "subject": "We go to HTML 5"
            }
        ]

