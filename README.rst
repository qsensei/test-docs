Test Sphinx Documentation
*************************

Installation
============

First off, you'll need to clone this project

.. sourcecode:: bash

    git clone git@bitbucket.org:qsensei/test-docs.git

You'll also need ``virtualenv``.

Mac OSX
----------

.. sourcecode:: bash

    pip install virtualenv

Linux
-----

.. sourcecode:: bash

    apt-get install python-virtualenv


Next, create a virtual environment at ``./env``, load it, and run the
installation script

.. sourcecode:: bash

    # Create the virtual environment
    virtualenv env
    # Load the virtual environment
    source env/bin/activate
    # Run the installation script - this will install some thing in your virtual
    # environment i.e. not mess up your configuration
    sh ./install.sh

Finally, run ``make customhtml`` to generate the HTML files at
``build/customhtml``.

.. note::

    What virtualenv does is create a virtual Python environment separate of
    your system environment (so you don't mess that up).

    To activate your virtual environment, run

    .. sourcecode::

        source path/to/my/env/bin/activate

    This will temporarily register the virtual Python environment as your
    default Python environment.


    To deactivate your virtual environment, run

    .. sourcecode:: bash

        deactivate


Configuration
=============

Configuration file
------------------

The configuration file is located at ``source/conf.py``. Note that this is a
Python script so this does have to be valid Python. The only important lines
for this project are:

.. sourcecode:: python

    extensions = ['sphinxcontrib.httpdomain']

This loads the httpdomain to allow for the special HTTP directives.

.. sourcecode:: python

    html_theme_path = ['themes']

This defines where the themes are located relative to the source folder.

.. sourcecode:: python

    html_theme = 'qsensei'

This defines what theme we are using (just the folder name in the above path).

Theme Creation
--------------
A bare-bones theme is creating with this and that is located at
``source/themes/qsensei``. The main configuration file is ``theme.conf``. The
``inherit`` key defines what theme to inherit from (in the case, the default theme).

``layout.html`` defines the main page.


Writing Documentation
=====================

Any ``source/*.rst`` file  will be converted to ``build/html/*.html``. Just
make sure your code is valid reStructuredText and all is good.

