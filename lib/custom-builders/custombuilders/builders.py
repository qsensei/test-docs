from sphinx.builders.html import StandaloneHTMLBuilder

class CustomBuilder(StandaloneHTMLBuilder):
    name = 'customhtml'

    def render_partial(self, node):
        # Do something to the nodes here
        result = StandaloneHTMLBuilder.render_partial(self, node)
        return result

def setup(app):
    app.add_builder(CustomBuilder)

